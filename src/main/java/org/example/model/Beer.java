package org.example.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Beer {

    private String beerId;
    private String beerName;

    @Override
    public String toString() {
        return "Beer Id Is: "+getBeerId()+" Beer Name Is:"+getBeerName();
    }
}
