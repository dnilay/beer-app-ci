package org.example.factory;

import org.example.model.Beer;

import java.util.Collection;

public interface BeerFactory {

    public String addBeer();
    public Collection<Beer> getAllBeers();
}
